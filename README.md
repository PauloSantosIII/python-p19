# Daily Practice
## Hoje você utilizará os conhecimentos adquiridos com o material e a demo, para criar o mundo mágico de Harry Potter.

Para começar, crie o arquivo main.py

 
## **Classe Professor:**
### **Atributos:**
* name: string
* class_name: string
* is_teaching: boolean

### **Construtor:**
* Deve receber como parâmetro e setar como atributo:
** name
** class_name

### **Métodos:**
* teach(self)
** Parâmetros: Não recebe parâmetros
** Procedimento: Alterar o atributo is_teaching para True
** Retorno: Esta função não tem retorno
* stop_teaching(self)
** Parâmetros: Não recebe parâmetros
** Procedimento: Alterar o atributo is_teaching para False
** Retorno: Esta função não tem retorno
 
## **Classe Student:**
### **Atributos:**
* name: string
* class_list: list
* school_year: integer
* age: integer
* is_attending_class: boolean

### **Construtor:**
* Deve receber como parâmetro e setar como atributo:
** name
** school_year
**  age

### **Métodos:**
* add_class(self, class_name)
** Parâmetros: Nome da disciplina a qual deseja-se adicionar na grade do estudante
** Procedimento: Adicionar a disciplina a lista de disciplinas class_list
** Retorno: Lista de disciplinas em class_list atualizada
* attend_class(self)
** Parâmetros: Não recebe parâmetros
** Procedimento: Alterar o atributo is_attending_class para True
** Retorno: Esta função não tem retorno
* stop_attend_class(self)
** Parâmetros: Não recebe parâmetros
** Procedimento: Alterar o atributo is_attending_class para False
** Retorno: Esta função não tem retorno
