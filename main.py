class Professor:
    def __init__(self, name=str, class_name=str, is_teaching=bool):
        self.name = name
        self.class_name = class_name

    def teach(self):
        self.is_teaching = True

    def stop_teaching(self):
        self.is_teaching = False

class Student:
    def __init__(self, name='', school_year=int, age=int, class_list=[], is_attending_class=bool):
        self.name = name
        self.school_year = school_year 
        self.age = age
        self.class_list = class_list

    def add_class(self, class_name=str):
        self.class_list.append(class_name)

    def attend_class(self):
        self.is_attending_class = True

    def stop_attend_class(self):
        self.is_attending_class = False
